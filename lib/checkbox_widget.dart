import 'package:flutter/material.dart';

Widget checkBox(String title, bool value, ValueChanged<bool?>? onChange){
  return Container(
              padding: EdgeInsets.only(left: 16.0,right: 16.0,bottom: 8.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(title),
                      Expanded(
                        child: Container(),
                      ),
                      Checkbox(
                          value: value,
                          onChanged: onChange)
                    ],
                  ),
                  Divider()
                ],
              ));

}

class CheckBoxWidget extends StatefulWidget {
  CheckBoxWidget({Key? key}) : super(key: key);

  @override
  _CheckBoxWidgetState createState() => _CheckBoxWidgetState();
}

class _CheckBoxWidgetState extends State<CheckBoxWidget> {
  bool check1 = false;
   bool check2 = false;
    bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("ComboBox")),
      body: ListView(
        children: [
          checkBox('Check 1', check1,(value){
            setState(() {
              check1 = value!;
            });
          }),
             checkBox('Check 2', check2,(value){
            setState(() {
              check2 = value!;
            });
          }),
             checkBox('Check 3', check3,(value){
            setState(() {
              check3 = value!;
            });
          }),
            TextButton.icon(onPressed: (){
            ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text('check1: $check1,check2: $check2,check3: $check3')));
         }, icon: Icon(Icons.save), label: Text('Save'))
        ],
      ),
    );
  }
}
