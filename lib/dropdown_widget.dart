import 'package:flutter/material.dart';

class DropdownWidget extends StatefulWidget {
  DropdownWidget({Key? key}) : super(key: key);

  @override
  _DropdownWidgetState createState() => _DropdownWidgetState();
}

class _DropdownWidgetState extends State<DropdownWidget> {
  String vaccine = '-';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(title: Text('Dropdown'),),
       body: Column(children: [
         Container(
           child: Row(
             children: [
               Text('Vaccine'),
               Expanded(child: Container()),
               DropdownButton(
                 items: [
                   DropdownMenuItem(child: Text('-'),value: '-',),
                   DropdownMenuItem(child: Text('Pfizer'),value: 'Pfizer',),
                   DropdownMenuItem(child: Text('Johnson & Johnson'),value: 'Johnson & Johnson',),
                   DropdownMenuItem(child: Text('Astrazeneca'),value: 'Astrazeneca',),
                   DropdownMenuItem(child: Text('Sinovac'),value: 'Sinovac',),
                 ],
                value: vaccine,
                onChanged: (String? value){
                  setState(() {
                    vaccine = value!;
                  });
                },
                 )
             ],
           ),
         ),
         Center(child: Text(vaccine, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)) )
       ],),
    );
  }
}